#pragma once

#include <string>
#include <map>
#include <syslog.h>

#define DSD_LOG(level, format, args...) \
	Logger::_Log(level, __FILE__, __LINE__, format, ##args);

#define DSD_LOG_ERROR(format, args...)  DSD_LOG(LOG_ERR, format, ##args)
#define DSD_LOG_WARN(format, args...)   DSD_LOG(LOG_WARNING, format, ##args)
#define DSD_LOG_NOTICE(format, args...) DSD_LOG(LOG_NOTICE, format, ##args)
#define DSD_LOG_INFO(format, args...)   DSD_LOG(LOG_INFO, format, ##args)
#define DSD_LOG_DEBUG(format, args...)  DSD_LOG(LOG_DEBUG, format, ##args)


class Logger {
	public:
		static void _Init(bool bVerbose, bool bDaemon = false);
		static void _Fini();

		template <typename ...Types>
		static void _Log(int level, const char* pszFile, int line,
				const char* pszFormat, Types... args);
}; // class Logger


template <typename ...Types>
/*static*/ void
Logger::_Log(int level, const char* pszFile, int line,
		const char* pszFormat, Types... args)
{
	static const std::map<int, std::string> s_header = {
		{ LOG_ERR,     "<ERROR>"  },
		{ LOG_WARNING, "<WARN>"   },
		{ LOG_NOTICE,  "<NOTICE>" },
		{ LOG_INFO,    "<INFO>"   },
		{ LOG_DEBUG,   "<DEBUG>"  }
	};
	std::string format;

	format += "(%15.15s:%3d) %-9s";
	format += pszFormat;

	syslog(level, format.c_str(), pszFile, line, s_header.at(level).c_str(),
			args...);
}

