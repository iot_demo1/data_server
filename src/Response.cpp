#include "Response.hpp"

#include <microhttpd.h>


Response::Response()
{
	m_pResponse = MHD_create_response_from_buffer(
			0, nullptr, MHD_RESPMEM_PERSISTENT);

	if (!m_pResponse)
		THROW_RUNTIME_ERROR("MHD_create_response_from_buffer() failed.");
}

Response::Response(const std::string& body)
{
	m_pResponse = MHD_create_response_from_buffer(
			body.length(), const_cast<char*>(body.c_str()),
			MHD_RESPMEM_MUST_COPY);

	if (!m_pResponse)
		THROW_RUNTIME_ERROR("MHD_create_response_from_buffer() failed.");
}

Response::~Response()
{
	if (m_pResponse) {
		MHD_destroy_response(m_pResponse);
		m_pResponse = nullptr;
	}
}


bool
Response::AddHeader(const std::string& key, const std::string& value)
{
	if (MHD_add_response_header(m_pResponse,
				key.c_str(), value.c_str()) != MHD_YES) {
		DSD_LOG_WARN("MHD_add_response_header() failed.");
		return false;
	}

	return true;
}

