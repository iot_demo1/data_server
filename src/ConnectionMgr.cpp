#include "ConnectionMgr.hpp"
#include "Connection.hpp"


// static class member variable declaration
/*static*/ ConnectionMgr* ConnectionMgr::s_pInst = nullptr;


/*static*/ ConnectionMgr*
ConnectionMgr::_GetInstance()
{
	if (!s_pInst)
		s_pInst = new ConnectionMgr;

	return s_pInst;
}

/*static*/ void
ConnectionMgr::_DeleteInstance()
{
	if (!s_pInst)
		return;

	delete s_pInst;
	s_pInst = nullptr;
}

void
ConnectionMgr::SetListener(IConnectionListener* pIListener)
{
	m_pIListener = pIListener;
}

bool
ConnectionMgr::Start(uint16_t port)
{
	DSD_LOG_DEBUG("Starting ConnectionMgr with port #%hu...", port);

	// register a handler for fatal error
	MHD_set_panic_func(&ConnectionMgr::_panicHandler, this);

	// start microhttp daemon
	m_pDaemon = MHD_start_daemon(MHD_USE_SELECT_INTERNALLY, port,
			nullptr, nullptr,
			&ConnectionMgr::_accessHandler, this,
			MHD_OPTION_NOTIFY_COMPLETED, &ConnectionMgr::_requestCompleted, this,
			MHD_OPTION_END);
	if (!m_pDaemon) {
		DSD_LOG_ERROR("Failed to start HTTP daemon!");
		return false;
	}

	// invoke listener callback
	if (m_pIListener)
		m_pIListener->OnStarted();

	return true;
}

void
ConnectionMgr::Stop()
{
	DSD_LOG_DEBUG("Stopping ConnectionMgr ...");

	// stop microhttp daemon
	if (m_pDaemon) {
		MHD_stop_daemon(m_pDaemon);
		m_pDaemon = nullptr;
	}

	// invoke listener callback
	if (m_pIListener)
		m_pIListener->OnStopped();
}


ConnectionMgr::ConnectionMgr()
	: m_pDaemon    { nullptr }
	, m_pIListener { nullptr }
{
	DSD_LOG_INFO("ConnectionMgr instance created.");
}

void
ConnectionMgr::panicHandler(const std::string& file, uint32_t line,
		const std::string& reason)
{
	// invoke OnPanic listener method
	if (m_pIListener)
		m_pIListener->OnPanic(file, line, reason);
}

int
ConnectionMgr::accessHandler(MHD_Connection* pConn,
		const std::string& url, const std::string& method,
		const char* pUploadData, size_t* pcbUploadData,
		void** ppConCls)
{
	Connection* pConnection;

	////////////////////////////////////////////////////////
	// at the case of new request
	if (!*ppConCls) {
		pConnection = new Connection(pConn, url, method);

		// invoke OnConnected listener method
		if (m_pIListener) {
			if (!m_pIListener->OnConnected(pConnection))
				return MHD_NO;
		}

		*ppConCls = pConnection; // This is IMPORTANT!
	}
	////////////////////////////////////////////////////////
	// at the case of existing request
	else {
		pConnection = reinterpret_cast<Connection*>(*ppConCls);

		// on streaming
		if (*pcbUploadData != 0) {
			pConnection->PushBodyContent(pUploadData);
			*pcbUploadData = 0;
		}
		// streaming done
		else {
			// invoke OnReceiveDone listener method
			if (m_pIListener) {
				if (!m_pIListener->OnReceiveDone(pConnection))
					return MHD_NO;
			}
		}
	}

	return MHD_YES;
}

void
ConnectionMgr::requestCompleted(MHD_Connection* pConn,
		void** ppConCls, MHD_RequestTerminationCode eTermCode)
{
	if (eTermCode != MHD_REQUEST_TERMINATED_COMPLETED_OK)
		DSD_LOG_WARN("Request terminated abnormally (%d).", eTermCode);

	Connection* pConnection = reinterpret_cast<Connection*>(*ppConCls);

	if (pConnection) {
		// invoke OnCompleted listener method
		if (m_pIListener)
			m_pIListener->OnCompleted(pConnection, eTermCode);

		delete pConnection;
		*ppConCls = nullptr;
	}
}

