#pragma once

#include "Common.hpp"
#include "Interface.hpp"

#include <map>


class WebApiMgr {
	public:
		static WebApiMgr* _GetInstance();
		static void       _DeleteInstance();

		IWebApi* GetApiInterface(
				const std::string& url, const std::string& method) const;

	private:
		WebApiMgr();
		~WebApiMgr();

	private:
		static WebApiMgr* s_pInst;

		std::map<std::string, IWebApi*> m_apiList;
}; // class ApiMgr

