#include "DatabaseUtil.hpp"


Result::Row::Row()
{
}

Result::Row::Row(const MYSQL_FIELD* pFields, const MYSQL_ROW row, size_t nFields)
{
	for (size_t i = 0; i < nFields; ++i) {
		if (row[i])
			m_data.push_back(std::make_pair(pFields[i].name, row[i]));
	}
}

size_t
Result::Row::GetSize() const
{
	return m_data.size();
}

bool
Result::Row::IsMember(const std::string& name) const
{
	for (const auto& e: m_data) {
		if (e.first == name)
			return true;
	}

	return false;
}

const std::string&
Result::Row::GetName(size_t idx) const
{
	return m_data.at(idx).first;
}

const std::string&
Result::Row::GetValue(size_t idx) const
{
	return m_data.at(idx).second;
}

const std::string&
Result::Row::GetValue(const std::string& name) const
{
	for (const auto& e: m_data) {
		if (e.first == name)
			return e.second;
	}

	throw std::out_of_range("There is no field with name '" + name + "',");
}


Result::Result(MYSQL* pCon, const std::string& query)
{
	if (mysql_query(pCon, query.c_str()) == 0)
		m_bSucceed = true;
	else {
		m_bSucceed = false;
		m_error = mysql_error(pCon);

		DSD_LOG_WARN("mysql_query() failed.");
		return;
	}

	MYSQL_RES* pRes = mysql_use_result(pCon);

	// mysql_use_result may return NULL
	// in the case of non-query like INSERT or DELETE
	if (!pRes)
		return;

	MYSQL_FIELD* pFields = mysql_fetch_fields(pRes);
	uint32_t nFields = mysql_num_fields(pRes);

	if (nFields > 0) {
		for (uint32_t i = 0; i < nFields; ++i)
			m_fields.push_back(pFields[i].name);

		MYSQL_ROW row;

		while ((row = mysql_fetch_row(pRes)))
			m_rows.push_back(new Row(pFields, row, nFields));
	}
	else
		DSD_LOG_NOTICE("Not any fields are specified.");

	mysql_free_result(pRes);
}

Result::~Result()
{
	for (auto e: m_rows)
		delete e;

	m_rows.clear();
}

bool
Result::IsSucceeded() const
{
	return m_bSucceed;
}

const char*
Result::GetError() const
{
	return m_error.c_str();
}

size_t
Result::GetNumOfFields() const
{
	return m_fields.size();
}

const std::string&
Result::GetField(size_t idx) const
{
	return m_fields.at(idx);
}

size_t
Result::GetNumOfRows() const
{
	return m_rows.size();
}

const Result::Row&
Result::GetRow(size_t idx) const
{
	return *(m_rows.at(idx));
}

