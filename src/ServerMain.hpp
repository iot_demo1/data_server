#pragma once

#include "Common.hpp"
#include "Interface.hpp"


// ServerMain class declaration
class ServerMain {
	private:
		class ConnectionListener : public IConnectionListener {
			public:
				virtual void OnStarted();
				virtual void OnStopped();

				virtual void OnPanic(const std::string& file, uint32_t line,
						const std::string& reason);

				virtual bool OnConnected(IConnection* pIConnection);
				virtual bool OnReceiveDone(IConnection* pIConnection);
				virtual void OnCompleted(IConnection* pIConnection,
						int termCode);
		}; // class ConnectionListener

	public:
		static void _Run(uint16_t port);
		static void _Quit();

	private:
		ServerMain();
		virtual ~ServerMain();

		void run(uint16_t port);
		void quit();

	private:
		static ServerMain* s_pInst;

		ConnectionListener m_connListener;
		bool m_bLoop;
}; // class ServerMain

