#pragma once

#include "Common.hpp"
#include "Interface.hpp"

#include <microhttpd.h>


class Connection : public IConnection {
	public:
		Connection(MHD_Connection* pConn,
				const std::string& url, const std::string& method);

		void PushBodyContent(const std::string& content);

		// derived from IConnection

		virtual const std::string& GetUrl() const
		{
			return m_url;
		}
		virtual const std::string& GetMethod() const
		{
			return m_method;
		}
		virtual const std::string& GetUserName() const
		{
			return m_username;
		}
		virtual const std::string& GetPassword() const
		{
			return m_password;
		}
		virtual const KeyValueList& GetHeader() const
		{
			return m_header;
		}
		virtual const MultiKeyValueList& GetArguments() const
		{
			return m_arguments;
		}
		virtual const std::string& GetBody() const
		{
			return m_body;
		}

		virtual bool SendResponse(uint32_t statusCode);
		virtual bool SendResponse(uint32_t statusCode, const Response& response);

	private:
		static int _iterateValue(void* cls, MHD_ValueKind kind,
				const char* key, const char* value);

	private:
		MHD_Connection*   m_pConn;
		const std::string m_url;
		const std::string m_method;
		std::string       m_username;
		std::string       m_password;
		KeyValueList      m_header;
		MultiKeyValueList m_arguments;
		std::string       m_body;
}; // class Connection

