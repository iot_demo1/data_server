#pragma once

#include "Common.hpp"

#include <vector>
#include <mysql.h>


class Result {
	public:
		class Row {
			public:
				Row();
				Row(const MYSQL_FIELD* pFields, const MYSQL_ROW row,
						size_t nFields);

				size_t GetSize() const; // get the number of fields
				bool IsMember(const std::string& name) const;
				const std::string& GetName(size_t idx) const;

				const std::string& GetValue(size_t idx) const;
				const std::string& operator[](size_t idx) const
				{
					return GetValue(idx);
				}

				const std::string& GetValue(const std::string& name) const;
				const std::string& operator[](const std::string& name) const
				{
					return GetValue(name);
				}

			private:
				std::vector<std::pair<std::string, std::string>> m_data;
		}; // class Result::Row

	public:
		Result(MYSQL* pCon, const std::string& query);
		~Result();

		// get result
		bool IsSucceeded() const;
		const char* GetError() const;

		// operation for fields
		size_t GetNumOfFields() const;
		const std::string& GetField(size_t idx) const;

		// operation for rows
		size_t GetNumOfRows() const;
		const Row& GetRow(size_t idx) const;

		const Row& operator[](size_t idx) const
		{
			return GetRow(idx);
		}

	private:
		bool        m_bSucceed;
		std::string m_error;

		std::vector<std::string> m_fields;
		std::vector<Row*>        m_rows;
}; // class Result

