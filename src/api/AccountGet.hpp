#pragma once

#include "Common.hpp"
#include "Interface.hpp"
#include "DatabaseMgr.hpp"


namespace Account {

class Get : public IWebApi {
	public:
		////////////////////////////////////////////////////
		// derived from IWebApi

		/*virtual*/ bool IsSupportGetMethod() const;
		/*virtual*/ bool IsSupportPostMethod() const;

		/*virtual*/ bool Process(IConnection* pIConnection);

	private:
		static void _makeResponseBody(std::string& body, const Result& result);
}; // class Get

} // namespace Account

