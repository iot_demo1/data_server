#include "StoreGet.hpp"
#include "Util.hpp"

#include <memory>
#include <bitset>


namespace Store {

bool
Get::IsSupportGetMethod() const
{
	return true;
}

bool
Get::IsSupportPostMethod() const
{
	return false;
}

bool
Get::Process(IConnection* pIConnection)
{
	const auto args = pIConnection->GetArguments();
	const auto itrStoreId = args.find("store_id");

	if (itrStoreId == args.end()) {
		DSD_LOG_INFO("PRIMARY KEY argument required: store_id");
		return pIConnection->SendResponse(406); // Not Acceptable
	}

	const std::string& storeId = itrStoreId->second;

	const auto rangeType = args.equal_range("type");
	std::bitset<9> mask;

	if (rangeType.first == args.end())
		mask.set(); // if 'type' argument is skipped, default value is 'all'
	else {
		for (auto itr = rangeType.first; itr != rangeType.second; ++itr) {
			if (itr->second == "all")
				mask.set();
			else if (itr->second == "basic")
				mask.set(0);
			else if (itr->second == "picture")
				mask.set(1);
			else if (itr->second == "business_hour")
				mask.set(2);
			else if (itr->second == "feature")
				mask.set(3);
			else if (itr->second == "promotion")
				mask.set(4);
			else if (itr->second == "social")
				mask.set(5);
			else if (itr->second == "review")
				mask.set(6);
			else if (itr->second == "tag")
				mask.set(7);
			else if (itr->second == "extra")
				mask.set(8);
		}
	}

	int status;
	Json::FastWriter writer;
	Json::Value root;

	if (mask[0]) { // StoreBasic
		if (200 != (status = _appendBasic(root, storeId)))
			return pIConnection->SendResponse(status);
	}
	if (mask[1]) { // StorePicture
		if (200 != (status = _appendPictures(root, storeId)))
			return pIConnection->SendResponse(status);
	}
	if (mask[2]) { // StoreBusinessHour
		if (200 != (status = _appendBusinessHour(root, storeId)))
			return pIConnection->SendResponse(status);
	}
	if (mask[3]) { // StoreFeature
		if (200 != (status = _appendFeature(root, storeId)))
			return pIConnection->SendResponse(status);
	}
	if (mask[4]) { // StorePromotion
		if (200 != (status = _appendPromotions(root, storeId)))
			return pIConnection->SendResponse(status);
	}
	if (mask[5]) { // StoreSocial
		if (200 != (status = _appendSocial(root, storeId)))
			return pIConnection->SendResponse(status);
	}
	if (mask[6]) { // StoreReview
		if (200 != (status = _appendReviews(root, storeId)))
			return pIConnection->SendResponse(status);
	}
	if (mask[7]) { // StoreTag
		if (200 != (status = _appendTags(root, storeId)))
			return pIConnection->SendResponse(status);
	}
	if (mask[8]) { // StoreExtra
		if (200 != (status = _appendExtras(root, storeId)))
			return pIConnection->SendResponse(status);
	}

	std::string body { writer.write(root) };

	DSD_LOG_DEBUG("[HTTP response body] %s", body.c_str());

	Response response(body);

	response.AddHeader("Content-Type", "application/json");
	return pIConnection->SendResponse(status, response);
}


/*static*/ int
Get::_appendBasic(Json::Value& root, const std::string& storeId)
{
	std::string query;
	Json::Value basic;

	_makeQuery(query, "StoreBasic", storeId);

	std::unique_ptr<Result> pResult(DatabaseMgr::_GetInstance()->Query(query));

	if (!pResult->IsSucceeded()) {
		DSD_LOG_WARN("Database error: %s", pResult->GetError());
		return 500; // Internal Server Error
	}

	if (pResult->GetNumOfRows() == 0) {
		DSD_LOG_WARN("Not registered store ID.");
		return 406; // Not Acceptable
	}
	if (pResult->GetNumOfRows() > 1) {
		DSD_LOG_WARN("There could not be multiple stores with same ID.");
		return 500; // Internal Server Error
	}

	const Result::Row& row = (*pResult)[0];

	basic["category"]    = row["category"];
	basic["name"]        = row["name"];
	basic["description"] = row["description"];
	basic["address"]     = row["address"];
	basic["contact"]     = row["contact"];
	basic["score"]       = std::atof(row["score"].c_str());

	root["basic"] = basic;

	return 200; // OK
}

/*static*/ int
Get::_appendPictures(Json::Value& root, const std::string& storeId)
{
	std::string query;
	Json::Value array;
	Json::Value picture;

	_makeQuery(query, "StorePicture", storeId);

	std::unique_ptr<Result> pResult(DatabaseMgr::_GetInstance()->Query(query));

	if (!pResult->IsSucceeded()) {
		DSD_LOG_WARN("Database error: %s", pResult->GetError());
		return 500; // Internal Server Error
	}

	for (size_t i = 0; i < pResult->GetNumOfRows(); ++i) {
		const Result::Row& row = (*pResult)[i];

		picture["idx"]    = std::atoi(row["idx"].c_str());
		picture["author"] = row["author"];
		picture["data"]   = row["data"];

		array.append(picture);
	}

	root["pictures"] = array;

	return 200; // OK
}

/*static*/ int
Get::_appendBusinessHour(Json::Value& root, const std::string& storeId)
{
	std::string query;
	Json::Value array;
	Json::Value day;

	_makeQuery(query, "StoreBusinessHour", storeId);

	std::unique_ptr<Result> pResult(DatabaseMgr::_GetInstance()->Query(query));

	if (!pResult->IsSucceeded()) {
		DSD_LOG_WARN("Database error: %s", pResult->GetError());
		return 500; // Internal Server Error
	}

	for (size_t i = 0; i < pResult->GetNumOfRows(); ++i) {
		const Result::Row& row = (*pResult)[i];

		if (row.IsMember("day_of_week"))
			day["day_of_week"] = std::atoi(row["day_of_week"].c_str());
		day["open"]  = Util::_ConvStrToDateTime(row["open"]);
		day["close"] = Util::_ConvStrToDateTime(row["close"]);

		array.append(day);
	}

	root["business_hour"] = array;

	return 200; // OK
}

/*static*/ int
Get::_appendFeature(Json::Value& root, const std::string& storeId)
{
	std::string query;
	Json::Value feature;

	_makeQuery(query, "StoreFeature", storeId);

	std::unique_ptr<Result> pResult(DatabaseMgr::_GetInstance()->Query(query));

	if (!pResult->IsSucceeded()) {
		DSD_LOG_WARN("Database error: %s", pResult->GetError());
		return 500; // Internal Server Error
	}

	if (pResult->GetNumOfRows() > 1) {
		DSD_LOG_WARN("There could not be multiple features with same ID.");
		return 500; // Internal Server Error
	}

	if (pResult->GetNumOfRows() == 1) {
		const Result::Row& row = (*pResult)[0];

		if (row.IsMember("reservation"))
			feature["reservation"] = _asBool(row, "reservation");
		feature["credit_card"] = _asBool(row, "credit_card");
		if (row.IsMember("outdoor_seat"))
			feature["outdoor_seat"] = _asBool(row, "outdoor_seat");
		if (row.IsMember("smoking"))
			feature["smoking"] = _asBool(row, "smoking");
		feature["parking"] = _asBool(row, "parking");
		if (row.IsMember("wifi"))
			feature["wifi"] = _asBool(row, "wifi");
	}

	root["feature"] = feature;

	return 200; // OK
}

/*static*/ int
Get::_appendPromotions(Json::Value& root, const std::string& storeId)
{
	std::string query;
	Json::Value array;
	Json::Value promotion;

	_makeQuery(query, "StorePromotion", storeId);

	std::unique_ptr<Result> pResult(DatabaseMgr::_GetInstance()->Query(query));

	if (!pResult->IsSucceeded()) {
		DSD_LOG_WARN("Database error: %s", pResult->GetError());
		return 500; // Internal Server Error
	}

	for (size_t i = 0; i < pResult->GetNumOfRows(); ++i) {
		const Result::Row& row = (*pResult)[i];

		promotion["idx"]    = std::atoi(row["idx"].c_str());
		promotion["type"]   = row["type"];
		promotion["detail"] = row["detail"];
		if (row.IsMember("start"))
			promotion["start"] = Util::_ConvStrToDateTime(row["start"]);
		if (row.IsMember("end"))
			promotion["end"]   = Util::_ConvStrToDateTime(row["end"]);

		array.append(promotion);
	}

	root["promotion"] = array;

	return 200; // OK
}

/*static*/ int
Get::_appendSocial(Json::Value& root, const std::string& storeId)
{
	std::string query;
	Json::Value social;

	_makeQuery(query, "StoreSocial", storeId);

	std::unique_ptr<Result> pResult(DatabaseMgr::_GetInstance()->Query(query));

	if (!pResult->IsSucceeded()) {
		DSD_LOG_WARN("Database error: %s", pResult->GetError());
		return 500; // Internal Server Error
	}

	if (pResult->GetNumOfRows() > 1) {
		DSD_LOG_WARN("There could not be multiple social info with same ID.");
		return 500; // Internal Server Error
	}

	if (pResult->GetNumOfRows() == 1) {
		const Result::Row& row = (*pResult)[0];

		if (row.IsMember("web"))
			social["web"] = row["web"];
		if (row.IsMember("facebook"))
			social["facebook"] = row["facebook"];
		if (row.IsMember("twitter"))
			social["twitter"]  = row["twitter"];
	}

	root["social"] = social;

	return 200; // OK
}

/*static*/ int
Get::_appendReviews(Json::Value& root, const std::string& storeId)
{
	std::string query;
	Json::Value array;
	Json::Value review;

	_makeQuery(query, "StoreReview", storeId);

	std::unique_ptr<Result> pResult(DatabaseMgr::_GetInstance()->Query(query));

	if (!pResult->IsSucceeded()) {
		DSD_LOG_WARN("Database error: %s", pResult->GetError());
		return 500; // Internal Server Error
	}

	for (size_t i = 0; i < pResult->GetNumOfRows(); ++i) {
		const Result::Row& row = (*pResult)[i];

		review["idx"]     = std::atoi(row["idx"].c_str());
		review["author"]  = row["author"];
		review["content"] = row["content"];

		array.append(review);
	}

	root["reviews"] = array;

	return 200; // OK
}

/*static*/ int
Get::_appendTags(Json::Value& root, const std::string& storeId)
{
	std::string query;
	Json::Value array;
	Json::Value tag;

	_makeQuery(query, "StoreTag", storeId);

	std::unique_ptr<Result> pResult(DatabaseMgr::_GetInstance()->Query(query));

	if (!pResult->IsSucceeded()) {
		DSD_LOG_WARN("Database error: %s", pResult->GetError());
		return 500; // Internal Server Error
	}

	for (size_t i = 0; i < pResult->GetNumOfRows(); ++i) {
		const Result::Row& row = (*pResult)[i];

		tag["idx"]     = std::atoi(row["idx"].c_str());
		tag["author"]  = row["author"];
		tag["content"] = row["content"];

		array.append(tag);
	}

	root["tags"] = array;

	return 200; // OK
}

/*static*/ int
Get::_appendExtras(Json::Value& root, const std::string& storeId)
{
	std::string query;
	Json::Value array;
	Json::Value extra;

	_makeQuery(query, "StoreExtra", storeId);

	std::unique_ptr<Result> pResult(DatabaseMgr::_GetInstance()->Query(query));

	if (!pResult->IsSucceeded()) {
		DSD_LOG_WARN("Database error: %s", pResult->GetError());
		return 500; // Internal Server Error
	}

	for (size_t i = 0; i < pResult->GetNumOfRows(); ++i) {
		const Result::Row& row = (*pResult)[i];

		extra["idx"] = std::atoi(row["idx"].c_str());
		if (row.IsMember("image"))
			extra["image"] = row["image"];
		extra["detail"] = row["detail"];
		if (row.IsMember("price"))
			extra["price"] = std::atoi(row["price"].c_str());

		array.append(extra);
	}

	root["extras"] = array;

	return 200; // OK
}

/*static*/ void
Get::_makeQuery(std::string& query,
		const std::string& table, const std::string& storeId) 
{
	query = "SELECT * FROM ";
	query += table;
	query += " WHERE store_id=";
	query += storeId;
	query += ';';
}

} // namespace Store

