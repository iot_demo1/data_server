#include "StoreForUser.hpp"
#include "Util.hpp"


namespace Store {

/*static*/ bool
AddPictures::_createQueryCB(std::string& query,
		const std::string& storeId, const std::string& username,
		const Json::Value& root)
{
	if (!root.isArray()) {
		DSD_LOG_NOTICE("Invalid Json format.");
		return false;
	}

	Json::ArrayIndex nItem = root.size();

	if (nItem == 0)
		return false;

	query = "INSERT INTO StorePicture (store_id, author, data) VALUES ";

	for (Json::ArrayIndex i = 0; i < nItem; ++i) {
		query += '(';

		// store_id INT UNSIGNED NOT NULL
		query += storeId;

		// idx INT UNSIGNED NOT NULL AUTO_INCREMENT

		// author CHAR(40) NOT NULL
		query += ", \'" + username + '\'';

		// data MEDIUMBLOB NOT NULL
		query += ", \'" + root[i].asString() + "\'), ";
	}
	query.pop_back(); // remove trailing ' '
	query.pop_back(); // remove trailing ','
	query += ';';

	return true;
}


/*static*/ bool
DeletePicture::_createQueryCB(std::string& query,
		const std::string& storeId, const std::string& username,
		const std::string& idx)
{
	query = "DELETE FROM StorePicture";
	query += " WHERE store_id = " + storeId;
	if (username != "root")
		query += " AND author = \'" + username + '\'';
	query += " AND idx = " + idx;
	query += ';';

	return true;
}


/*static*/ bool
AddReview::_createQueryCB(std::string& query,
		const std::string& storeId, const std::string& username,
		const Json::Value& root)
{
	if (!root.isObject())
		return false;
	if (!root.isMember("content"))
		return false;

	query = "INSERT INTO StoreReview (store_id, author, content) VALUE (";

	// store_id INT UNSIGNED NOT NULL
	query += storeId;

	// idx INT UNSIGNED NOT NULL AUTO_INCREMENT

	// author CHAR(40) NOT NULL
	query += ", \'" + username + '\'';

	// content TINYTEXT NOT NULL
	query += ", \'" + root["content"].asString() + "\');";

	return true;
}


/*static*/ bool
DeleteReview::_createQueryCB(std::string& query,
		const std::string& storeId, const std::string& username,
		const std::string& idx)
{
	query = "DELETE FROM StoreReview";
	query += " WHERE store_id = " + storeId;
	if (username != "root")
		query += " AND author = \'" + username + '\'';
	query += " AND idx = " + idx;
	query += ';';

	return true;
}


/*static*/ bool
AddTag::_createQueryCB(std::string& query,
		const std::string& storeId, const std::string& username,
		const Json::Value& root)
{
	if (!root.isObject())
		return false;
	if (!root.isMember("content"))
		return false;

	query = "INSERT INTO StoreTag (store_id, author, content) VALUE (";

	// store_id INT UNSIGNED NOT NULL
	query += storeId;

	// idx INT UNSIGNED NOT NULL AUTO_INCREMENT

	// author CHAR(40) NOT NULL
	query += ", \'" + username + '\'';

	// content VARCHAR(30) NOT NULL
	query += ", \'" + root["content"].asString() + "\');";

	return true;
}


/*static*/ bool
DeleteTag::_createQueryCB(std::string& query,
		const std::string& storeId, const std::string& username,
		const std::string& idx)
{
	query = "DELETE FROM StoreTag";
	query += " WHERE store_id = " + storeId;
	if (username != "root")
		query += " AND author = \'" + username + '\'';
	query += " AND idx = " + idx;
	query += ';';

	return true;
}

} // namespace Store

