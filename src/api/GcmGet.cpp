#include "GcmGet.hpp"
#include "DatabaseMgr.hpp"

#include <memory>

#include <json/json.h>


namespace GCM {

bool
Get::IsSupportGetMethod() const
{
	return true;
}

bool
Get::IsSupportPostMethod() const
{
	return false;
}

bool
Get::Process(IConnection* pIConnection)
{
	const auto args = pIConnection->GetArguments();
	const auto itrStoreId = args.find("store_id");
	const auto itrMacAddr = args.find("mac_addr");

	if ((itrStoreId == args.end()) || (itrMacAddr == args.end())) {
		DSD_LOG_INFO("PRIMARY KEY argument required: mac_addr and store_id");
		return pIConnection->SendResponse(406); // Not Acceptable
	}

	// get user email and registration ID
	KeyValueList regInfos;

	if (!_getRegistraionInfos(regInfos, args))
		return pIConnection->SendResponse(500); // Internal Server Error

	// create response body
	std::string body;

	if (!_makeResponseBody(body, itrStoreId->second, regInfos))
		return pIConnection->SendResponse(500); // Internal Server Error

	// send response
	Response response(body);

	response.AddHeader("Content-Type", "application/json");
	return pIConnection->SendResponse(200, response);
}


/*static*/ bool
Get::_getRegistraionInfos(KeyValueList& regInfos, const MultiKeyValueList& args)
{
	std::string query("SELECT email, registration_id FROM Devices WHERE ");
	auto range = args.equal_range("mac_addr");
	auto begin = range.first;
	auto end   = range.second;

	if (begin != args.end()) {
		query += "mac_addr=\'" + (*begin++).second + '\'';

		for (auto itr = begin; itr != end; ++itr)
			query += " OR mac_addr=\'" + (*itr).second + '\'';

		query += ';';
	}

	std::unique_ptr<Result> pResult(
			DatabaseMgr::_GetInstance()->Query(query));

	if (!pResult->IsSucceeded()) {
		DSD_LOG_WARN("Database error: %s", pResult->GetError());
		return false;
	}

	for (size_t i = 0; i < pResult->GetNumOfRows(); ++i) {
		const Result::Row& row = (*pResult)[i];

		regInfos.emplace(std::make_pair(row["email"], row["registration_id"]));
	}

	return true;
}

/*static*/ bool
Get::_makeResponseBody(std::string& body,
		const std::string& storeId, const KeyValueList& regInfos)
{
	Json::FastWriter writer;
	Json::Value root;
	Json::Value regIds;

	// get store information
	std::string storeName;
	std::string category;

	if (!_getStoreInfo(storeName, category, storeId))
		return false;

	// registration id is added after applying favorites filter
	for (const auto& regInfo: regInfos) {
		const std::string& email = regInfo.first;
		const std::string& regId = regInfo.second;
		std::set<std::string> favorites;

		if (!_getFavorites(favorites, email))
			return false;

		if (favorites.find(category) != favorites.end())
			regIds.append(regId);
	}

	// get content of push message
	std::string msg;

	if (!_getMsg(msg, storeId))
		return false;

	// write body
	root["registration_ids"] = regIds;
	root["msg"] = '[' + storeName + "] " + msg;

	body = writer.write(root);

	DSD_LOG_DEBUG("[HTTP response body] %s", body.c_str());

	return true;
}

/*static*/ bool
Get::_getStoreInfo(std::string& storeName, std::string& category,
		const std::string& storeId)
{
	std::string query("SELECT name, category FROM StoreBasic WHERE store_id=");

	query += '\'' + storeId + "\';";

	std::unique_ptr<Result> pResult(
			DatabaseMgr::_GetInstance()->Query(query));

	if (!pResult->IsSucceeded()) {
		DSD_LOG_WARN("Database error: %s", pResult->GetError());
		return false;
	}

	switch (pResult->GetNumOfRows()) {
		case 0:
			DSD_LOG_INFO("Invalid store ID.");
			return false;
		case 1:
			{
				const Result::Row& row = (*pResult)[0];

				storeName = row["name"];
				category  = row["category"];
			}
			break;
		default:
			DSD_LOG_ERROR("There could not be multiple registration ID.");
			return false;
	}

	return true;
}

/*static*/ bool
Get::_getFavorites(std::set<std::string>& favorites, const std::string& email)
{
	std::string query("SELECT category FROM UserFavorites WHERE email=");

	query += '\'' + email + "\';";

	std::unique_ptr<Result> pResult(
			DatabaseMgr::_GetInstance()->Query(query));

	if (!pResult->IsSucceeded()) {
		DSD_LOG_WARN("Database error: %s", pResult->GetError());
		return false;
	}

	for (size_t i = 0; i < pResult->GetNumOfRows(); ++i)
		favorites.insert((*pResult)[i]["category"]);

	return true;
}

/*static*/ bool
Get::_getMsg(std::string& msg, const std::string& storeId)
{
	std::string query("SELECT detail FROM StorePromotion WHERE store_id=");

	query += '\'' + storeId + "\';";

	std::unique_ptr<Result> pResult(
			DatabaseMgr::_GetInstance()->Query(query));

	if (!pResult->IsSucceeded()) {
		DSD_LOG_WARN("Database error: %s", pResult->GetError());
		return false;
	}

	if (pResult->GetNumOfRows() == 0) {
		DSD_LOG_NOTICE("There is no promotion item now.");

		// default message
		msg = "등록하신 관심 카테고리 업소가 주변에서 검색 되었습니다.";
	}
	else
		msg = (*pResult)[0]["detail"];

	return true;
}

} // namespace GCM

