#pragma once

#include "Common.hpp"
#include "Interface.hpp"

#include <functional>

#include <json/json.h>


namespace Store {

class Base : public IWebApi {
	typedef std::function<bool (std::string& query,
			const std::string& storeId, const std::string& username,
			const Json::Value& root)>
		AddQueryCallback;
	typedef std::function<bool (std::string& query,
			const std::string& storeId, const std::string& username,
			const std::string& idx)>
		DeleteQueryCallback;
	typedef std::function<bool (std::string& query,
			const std::string& storeId, const std::string& username,
			const Json::Value& root)>
		ModifyQueryCallback;

	typedef std::function<bool (std::string& query,
			const std::string& storeId, const std::string& username)>
		GenericQueryCallback;

	public:
		////////////////////////////////////////////////////
		// derived from IWebApi

		// support HTTP GET?
		/*virtual*/ bool IsSupportGetMethod() const  { return false; }
		// support HTTP POST?
		/*virtual*/ bool IsSupportPostMethod() const { return true; }

	protected:
		// real implementations of processing requests
		bool processAdd(IConnection* pIConnection,
				const AddQueryCallback& callback);
		bool processDelete(IConnection* pIConnection,
				const DeleteQueryCallback& callback);
		bool processModify(IConnection* pIConnection,
				const ModifyQueryCallback& callback);

		bool processImpl(IConnection* pIConnection,
				const GenericQueryCallback& callback);

	private:
		Json::Reader m_reader;
}; // class Base

} // namespace Store

