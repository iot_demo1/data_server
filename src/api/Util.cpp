#include "Util.hpp"

#include <sstream>


/*static*/ Json::Value
Util::_ConvStrToDate(const std::string& str)
{
	Json::Value date;
	std::string::size_type idx;

	date["year"]  = std::atoi(str.c_str());

	idx = str.find('-') + 1;
	date["month"] = std::atoi(str.substr(idx).c_str());

	idx = str.find('-', idx) + 1;
	date["day"]   = std::atoi(str.substr(idx).c_str());

	return date;
}

/*static*/ Json::Value
Util::_ConvStrToTime(const std::string& str)
{
	Json::Value time;

	time["hour"]   = std::atoi(str.c_str());
	time["minute"] = std::atoi(str.substr(str.find(':') + 1).c_str());

	return time;
}

/*static*/ Json::Value
Util::_ConvStrToDateTime(const std::string& str)
{
	const std::string::size_type idx = str.find(' ');
	const Json::Value date { _ConvStrToDate(str.substr(0, idx)) };
	const Json::Value time { _ConvStrToTime(str.substr(idx + 1)) };

	Json::Value datetime;

	datetime["year"]   = date["year"];
	datetime["month"]  = date["month"];
	datetime["day"]    = date["day"];
	datetime["hour"]   = time["hour"];
	datetime["minute"] = time["minute"];

	return datetime;
}

/*static*/ std::string
Util::_ConvDateToStr(const Json::Value& val)
{
	std::string str;

	if (val.isObject()
			&& val.isMember("year")
			&& val.isMember("month")
			&& val.isMember("day")) {
		Date date;

		date.year  = val["year"].asUInt();
		date.month = val["month"].asUInt();
		date.day   = val["day"].asUInt();

		std::stringstream buf;

		buf << date.year << '-' << date.month << '-' << date.day;

		str = buf.str();
	}

	return str;
}

/*static*/ std::string
Util::_ConvTimeToStr(const Json::Value& val)
{
	std::string str;

	if (val.isObject()
			&& val.isMember("hour")
			&& val.isMember("minute")) {
		Time time;

		time.hour   = val["hour"].asUInt();
		time.minute = val["minute"].asUInt();

		std::stringstream buf;

		buf << time.hour << ':' << time.minute << ':' << "00";

		str = buf.str();
	}

	return str;
}

/*static*/ std::string
Util::_ConvDateTimeToStr(const Json::Value& val)
{
	const std::string date { _ConvDateToStr(val) };
	const std::string time { _ConvTimeToStr(val) };

	std::string str { date + ' ' + time };

	return str;
}

