#pragma once

#include "Common.hpp"

#include <json/json.h>


class Util {
	public:
		static Json::Value _ConvStrToDate(const std::string& str);
		static Json::Value _ConvStrToTime(const std::string& str);
		static Json::Value _ConvStrToDateTime(const std::string& str);

		static std::string _ConvDateToStr(const Json::Value& val);
		static std::string _ConvTimeToStr(const Json::Value& val);
		static std::string _ConvDateTimeToStr(const Json::Value& val);
}; // class Util

