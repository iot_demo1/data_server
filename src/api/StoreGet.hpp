#pragma once

#include "Common.hpp"
#include "Interface.hpp"
#include "DatabaseMgr.hpp"

#include <json/json.h>


namespace Store {

class Get : public IWebApi {
	public:
		////////////////////////////////////////////////////
		// derived from IWebApi

		/*virtual*/ bool IsSupportGetMethod() const;
		/*virtual*/ bool IsSupportPostMethod() const;

		/*virtual*/ bool Process(IConnection* pIConnection);

	private:
		static int _appendBasic(Json::Value& root, const std::string& storeId);
		static int _appendPictures(Json::Value& root,
				const std::string& storeId);
		static int _appendBusinessHour(Json::Value& root,
				const std::string& storeId);
		static int _appendFeature(Json::Value& root,
				const std::string& storeId);
		static int _appendPromotions(Json::Value& root,
				const std::string& storeId);
		static int _appendSocial(Json::Value& root,
				const std::string& storeId);
		static int _appendReviews(Json::Value& root,
				const std::string& storeId);
		static int _appendTags(Json::Value& root, const std::string& storeId);
		static int _appendExtras(Json::Value& root,
				const std::string& storeId);

		static void _makeQuery(std::string& query,
				const std::string& table, const std::string& storeId);

		static bool _asBool(const Result::Row& row, const std::string& name)
		{
			return (std::atoi(row[name].c_str()) ? true : false);
		}
}; // class Get

} // namespace Store

