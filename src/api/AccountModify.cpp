#include "AccountModify.hpp"
#include "DatabaseMgr.hpp"
#include "Util.hpp"

#include <memory>


namespace Account {

bool
Modify::IsSupportGetMethod() const
{
	return false;
}

bool
Modify::IsSupportPostMethod() const
{
	return true;
}

bool
Modify::Process(IConnection* pIConnection)
{
	const std::string& username = pIConnection->GetUserName();

	// username is email so it is the primary key for DB tables
	if (username.empty()) {
		DSD_LOG_INFO("HTTP authentication required.");
		return pIConnection->SendResponse(406); // Not Acceptable
	}

	// validate json body
	Json::Reader reader;
	Json::Value  root;
	std::string  dbQuery;

	if (!reader.parse(pIConnection->GetBody(), root)) {
		DSD_LOG_INFO("Failed to handle request due to malformed json syntax.");
		return pIConnection->SendResponse(400); // Bad Request
	}

	if (root.isMember("user")) {
		// add user information to Users table
		_makeUserQuery(dbQuery, username, root["user"]);

		std::unique_ptr<Result> pResUser(
				DatabaseMgr::_GetInstance()->Query(dbQuery));

		if (!pResUser->IsSucceeded()) {
			DSD_LOG_WARN("Database error: %s", pResUser->GetError());
			return pIConnection->SendResponse(500); // Internal Server Error
		}
	}
	
	if (root.isMember("device")) {
		// add MAC address and registration ID to Devices table
		_makeDeviceQuery(dbQuery, username, root["device"]);

		std::unique_ptr<Result> pResDevice(
				DatabaseMgr::_GetInstance()->Query(dbQuery));

		if (!pResDevice->IsSucceeded()) {
			DSD_LOG_WARN("Database error: %s", pResDevice->GetError());
			return pIConnection->SendResponse(500); // Internal Server Error
		}
	}

	return pIConnection->SendResponse(200); // OK
}


/*static*/ void
Modify::_makeUserQuery(std::string& query,
		const std::string& username, const Json::Value& user)
{
	bool bFirstElem = true;

	query = "UPDATE Users SET ";

	// sex ENUM('male', 'female')
	if (user.isMember("sex")) {
		query += "sex = \'" + user["sex"].asString() + '\'';
		bFirstElem = false;
	}

	// birth_date DATE
	if (user.isMember("birth_date")) {
		if (bFirstElem)
			bFirstElem = false;
		else
			query += ", ";

		query += "birth_date = \'" + Util::_ConvDateToStr(user["birth_date"])
			+ '\'';
	}

	// mobile CHAR(30)
	if (user.isMember("mobile")) {
		if (!bFirstElem)
			query += ", ";

		query += "mobile = \'" + user["mobile"].asString() + '\'';
	}

	// add email condition if user is not root
	if (username != "root")
		query += " WHERE email = \'" + username + '\'';
	query += ';';
}

/*static*/ void
Modify::_makeDeviceQuery(std::string& query,
		const std::string& username, const Json::Value& device)
{
	bool bFirstElem = true;

	query = "UPDATE Devices SET ";

	// mac_addr CHAR(12) NOT NULL UNIQUE KEY
	if (device.isMember("mac_addr")) {
		query += "mac_addr = \'" + device["mac_addr"].asString() + '\'';
		bFirstElem = false;
	}

	// registration_id VARCHAR(4096) NOT NULL
	if (device.isMember("registration_id")) {
		if (!bFirstElem)
			query += ", ";

		query += "registration_id = \'" + device["registration_id"].asString()
			+ '\'';
	}

	// add email condition if user is not root
	if (username != "root")
		query += " WHERE email = \'" + username + '\'';
	query += ';';
}

} // namespace Account

