#include "StoreForOwner.hpp"
#include "Util.hpp"

#include <sstream>


namespace Store {

/*static*/ bool
ModifyBasic::_createQueryCB(std::string& query,
		const std::string& storeId, const std::string& username,
		const Json::Value& root)
{
	if (!root.isObject()) {
		DSD_LOG_NOTICE("Invalid Json format.");
		return false;
	}

	const std::string columns[] = {
		"category", "name", "description", "address", "contact"
	};
	bool bFirstElem = true;

	query = "UPDATE StoreBasic SET ";

	for (auto column: columns) {
		if (root.isMember(column)) {
			if (bFirstElem)
				bFirstElem = false;
			else
				query += ", ";

			query += column + " = \'" + root[column].asString() + '\'';
		}
	}

	query += " WHERE store_id = " + storeId;
	if (username != "root")
		query += " AND owner = \'" + username + '\'';
	query += ';';

	return true;
}


/*static*/ bool
ModifyBusinessHour::_createQueryCB(std::string& query,
		const std::string& storeId, const std::string& username,
		const Json::Value& root)
{
	if (!root.isArray()) {
		DSD_LOG_NOTICE("Invalid Json format.");
		return false; // Bad Request
	}

	std::string colNames;
	std::string valExprs;

	query = "DELETE FROM StoreBusinessHour WHERE ";
	if (username == "root")
		query += "store_id = " + storeId;
	else {
		query += "(SELECT store_id FROM StoreBasic WHERE ";
		query += "store_id = " + storeId;
		query += " AND owner = " + '\'' + username + "\')";
	}
	query += ';';

	Json::ArrayIndex nItem = root.size();

	for (Json::ArrayIndex i = 0; i < nItem; ++i) {
		const Json::Value& item = root[i];

		if (!item.isMember("open") || !item.isMember("close"))
			return false;

		// store_id INT UNSIGNED NOT NULL
		colNames += "store_id";
		valExprs += storeId;

		// day_of_week BIT(7)
		if (item.isMember("day_of_week")) {
			colNames += ", day_of_week";
			valExprs += ", " + item["day_of_week"].asUInt();
		}

		// open TIME NOT NULL
		colNames += ", open";
		valExprs += ", \'" + Util::_ConvTimeToStr(item["open"]) + '\'';

		// close TIME NOT NULL
		colNames += ", close";
		valExprs += ", \'" + Util::_ConvTimeToStr(item["close"]) + '\'';

		// [TODO] check username has right authority
		query += " INSERT INTO StoreBusinessHour (" + colNames + ") VALUE ("
			+ valExprs + ");";
	}

	return true;
}


/*static*/ bool
ModifyFeature::_createQueryCB(std::string& query,
		const std::string& storeId, const std::string& username,
		const Json::Value& root)
{
	if (!root.isObject()) {
		DSD_LOG_NOTICE("Invalid Json format.");
		return false;
	}

	const std::string columns[] = {
		"reservation", "credit_card", "outdoor_seat", "smoking", "parking",
		"wifi"
	};
	bool bFirstElem = true;
	std::stringstream buf;

	buf << "UPDATE StoreFeature SET ";

	for (auto e: columns) {
		if (root.isMember(e)) {
			if (bFirstElem)
				bFirstElem = false;
			else
				buf << ", ";

			buf << e << " = \'" << root[e].asInt() << '\'';
		}
	}

	buf << " WHERE ";
	if (username == "root")
		buf << "store_id = " << storeId;
	else {
		buf << "(SELECT store_id FROM StoreBasic WHERE ";
		buf << "store_id = " << storeId;
		buf << " AND owner = " << '\'' << username << "\')";
	}
	buf << ';';

	query = buf.str();

	return true;
}


/*static*/ bool
AddPromotion::_createQueryCB(std::string& query,
		const std::string& storeId, const std::string& username,
		const Json::Value& root)
{
	if (!root.isObject())
		return false;
	if (!root.isMember("type") || !root.isMember("detail"))
		return false;

	std::string colNames;
	std::string valExprs;

	// store_id INT UNSIGNED NOT NULL
	colNames += "store_id";
	valExprs += storeId;

	// idx INT UNSIGNED NOT NULL AUTO_INCREMENT

	// type ENUM('coupon', 'sale', 'gift', 'event', 'others') NOT NULL
	colNames += ", type";
	valExprs += ", \'" + root["type"].asString() + '\'';

	// detail CHAR(40) NOT NULL
	colNames += ", detail";
	valExprs += ", \'" + root["detail"].asString() + '\'';

	// start DATETIME
	std::string start;

	if (root.isMember("start")) {
		colNames += ", start";
		valExprs += ", \'" + Util::_ConvDateTimeToStr(root["start"]) + '\'';
	}

	// end DATETIME
	std::string end;

	if (root.isMember("end")) {
		colNames += ", end";
		valExprs += ", \'" + Util::_ConvDateTimeToStr(root["end"]) + '\'';
	}

	// [TODO] verify the current username is store owner
	query = "INSERT INTO StorePromotion (" + colNames + ")"
		+ " VALUE (" + valExprs + ");";

	return true;
}


/*static*/ bool
DeletePromotion::_createQueryCB(std::string& query,
		const std::string& storeId, const std::string& username,
		const std::string& idx)
{
	query = "DELETE FROM StorePromotion WHERE ";
	if (username == "root")
		query += "store_id = " + storeId;
	else {
		query += "(SELECT store_id FROM StoreBasic WHERE ";
		query += "store_id = " + storeId;
		query += " AND owner = " + '\'' + username + "\')";
	}
	query += " AND idx = " + idx + ';';

	return true;
}


/*static*/ bool
ModifySocial::_createQueryCB(std::string& query,
		const std::string& storeId, const std::string& username,
		const Json::Value& root)
{
	if (!root.isObject()) {
		DSD_LOG_NOTICE("Invalid Json format.");
		return false;
	}

	const std::string columns[] = { "web", "facebook", "twitter" };
	bool bFirstElem = true;

	query = "UPDATE StoreSocial SET ";

	for (auto e: columns) {
		if (root.isMember(e)) {
			if (bFirstElem)
				bFirstElem = false;
			else
				query += ", ";

			query += e + " = \'" + root[e].asString() + '\'';
		}
	}

	query += " WHERE ";
	if (username == "root")
		query += "store_id = " + storeId;
	else {
		query += "(SELECT store_id FROM StoreBasic WHERE ";
		query += "store_id = " + storeId;
		query += " AND owner = " + '\'' + username + "\')";
	}
	query += ';';

	return true;
}


bool
AddExtra::_createQueryCB(std::string& query,
		const std::string& storeId, const std::string& username,
		const Json::Value& root)
{
	if (!root.isObject())
		return false;
	if (!root.isMember("detail"))
		return false;

	std::string colNames;
	std::string valExprs;

	// store_id INT UNSIGNED NOT NULL
	colNames += "store_id";
	valExprs += storeId;

	// idx INT UNSIGNED NOT NULL AUTO_INCREMENT

	// image MEDIUMBLOB
	if (root.isMember("image")) {
		colNames += ", image";
		valExprs += ", \'" + root["image"].asString() + '\'';
	}

	// detail TINYTEXT NOT NULL
	colNames += ", detail";
	valExprs += ", \'" + root["detail"].asString() + '\'';

	// price INT UNSIGNED
	if (root.isMember("price")) {
		colNames += ", price";
		valExprs += ", " + root["price"].asInt();
	}

	// [TODO] verify the current username is store owner
	query =
		"INSERT INTO StoreExtra (" + colNames + ") VALUE (" + valExprs + ");";

	return true;
}


/*static*/ bool
DeleteExtra::_createQueryCB(std::string& query,
		const std::string& storeId, const std::string& username,
		const std::string& idx)
{
	query = "DELETE FROM StoreExtra WHERE ";
	if (username == "root")
		query += "store_id = " + storeId;
	else {
		query += "(SELECT store_id FROM StoreBasic WHERE ";
		query += "store_id = " + storeId;
		query += " AND owner = " + '\'' + username + "\')";
	}
	query += " AND idx = " + idx + ';';

	return true;
}

} // namespace Store

