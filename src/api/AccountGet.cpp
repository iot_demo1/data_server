#include "AccountGet.hpp"
#include "Util.hpp"

#include <memory>

#include <json/json.h>


namespace Account {

bool
Get::IsSupportGetMethod() const
{
	return true;
}

bool
Get::IsSupportPostMethod() const
{
	return false;
}

bool
Get::Process(IConnection* pIConnection)
{
	std::string dbQuery;
	std::string body;

	const auto args = pIConnection->GetArguments();
	const auto itrEmail = args.find("email");

	if (itrEmail == args.end()) {
		DSD_LOG_INFO("PRIMARY KEY argument required: email");
		return pIConnection->SendResponse(406); // Not Acceptable
	}

	// do DB jobs for Devices table
	dbQuery = "SELECT * FROM Users WHERE email=";
	dbQuery += '\'' + (*itrEmail).second + "\';";

	std::unique_ptr<Result> pResult(
			DatabaseMgr::_GetInstance()->Query(dbQuery));

	if (!pResult->IsSucceeded()) {
		DSD_LOG_WARN("Database error: %s", pResult->GetError());
		return pIConnection->SendResponse(500); // Internal Server Error
	}

	size_t nFound = pResult->GetNumOfRows();
	
	if (nFound == 0) {
		DSD_LOG_DEBUG("Not registered email.");
		return pIConnection->SendResponse(406); // Not Acceptable
	}
	if (nFound > 1) {
		DSD_LOG_ERROR("There could no be multiple users with same email.");
		return pIConnection->SendResponse(500); // Internal Server Error
	}

	_makeResponseBody(body, *pResult);
	DSD_LOG_DEBUG("[HTTP response body] %s", body.c_str());

	Response response(body);

	response.AddHeader("Content-Type", "application/json");
	return pIConnection->SendResponse(200, response);
}


/*static*/ void
Get::_makeResponseBody(std::string& body, const Result& result)
{
	const Result::Row& row = result[0];
	Json::FastWriter writer;
	Json::Value root;

	// email CHAR(40) NOT NULL PRIMARY KEY
	root["email"] = row["email"];

	// name CHAR(40) NOT NULL
	root["name"]  = row["name"];

	// sex ENUM('male', 'female')
	if (row.IsMember("sex"))
		root["sex"] = row["sex"];

	// birth_date DATE
	if (row.IsMember("birth_date")) {
		root["birth_date"] =
			Util::_ConvStrToDate(row["birth_date"]);
	}

	// mobile CHAR(30)
	if (row.IsMember("mobile"))
		root["mobile"] = row["mobile"];

	body = writer.write(root);
}

} // namespace Account

