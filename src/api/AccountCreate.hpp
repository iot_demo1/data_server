#pragma once

#include "Common.hpp"
#include "Interface.hpp"

#include <json/json.h>


namespace Account {

class Create : public IWebApi {
	public:
		////////////////////////////////////////////////////
		// derived from IWebApi

		/*virtual*/ bool IsSupportGetMethod() const;
		/*virtual*/ bool IsSupportPostMethod() const;

		/*virtual*/ bool Process(IConnection* pIConnection);

	private:
		static bool _makeUserQuery(std::string& query,
				const Json::Value& user);
		static bool _makeDeviceQuery(std::string& query,
				const std::string& email, const Json::Value& device);
}; // class Create

} // namespace Account

