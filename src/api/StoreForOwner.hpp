#pragma once

#include "StoreBase.hpp"


namespace Store {

class ModifyBasic : public Base {
	public:
		////////////////////////////////////////////////////
		// derived from IWebApi

		/*virtual*/ bool Process(IConnection* pIConnection)
		{
			return processModify(pIConnection, &ModifyBasic::_createQueryCB);
		}

	private:
		static bool _createQueryCB(std::string& query,
				const std::string& storeId, const std::string& username,
				const Json::Value& root);
}; // class ModifyBasic


class ModifyBusinessHour : public Base {
	public:
		////////////////////////////////////////////////////
		// derived from IWebApi

		/*virtual*/ bool Process(IConnection* pIConnection)
		{
			return processModify(pIConnection, &ModifyBusinessHour::_createQueryCB);
		}

	private:
		static bool _createQueryCB(std::string& query,
				const std::string& storeId, const std::string& username,
				const Json::Value& root);
}; // class ModifyBusinessHour


class ModifyFeature : public Base {
	public:
		////////////////////////////////////////////////////
		// derived from IWebApi

		/*virtual*/ bool Process(IConnection* pIConnection)
		{
			return processModify(pIConnection, &ModifyFeature::_createQueryCB);
		}

	private:
		static bool _createQueryCB(std::string& query,
				const std::string& storeId, const std::string& username,
				const Json::Value& root);
}; // class ModifyFeature


class AddPromotion : public Base {
	public:
		////////////////////////////////////////////////////
		// derived from IWebApi

		/*virtual*/ bool Process(IConnection* pIConnection)
		{
			return processAdd(pIConnection, &AddPromotion::_createQueryCB);
		}

	private:
		static bool _createQueryCB(std::string& query,
				const std::string& storeId, const std::string& username,
				const Json::Value& root);
}; // class AddPromotion


class DeletePromotion : public Base {
	public:
		////////////////////////////////////////////////////
		// derived from IWebApi

		/*virtual*/ bool Process(IConnection* pIConnection)
		{
			return processDelete(pIConnection, &DeletePromotion::_createQueryCB);
		}

	private:
		static bool _createQueryCB(std::string& query,
				const std::string& storeId, const std::string& username,
				const std::string& idx);
}; // class DeletePromotion


class ModifySocial : public Base {
	public:
		////////////////////////////////////////////////////
		// derived from IWebApi

		/*virtual*/ bool Process(IConnection* pIConnection)
		{
			return processModify(pIConnection, &ModifySocial::_createQueryCB);
		}

	private:
		static bool _createQueryCB(std::string& query,
				const std::string& storeId, const std::string& username,
				const Json::Value& root);
}; // class ModifySocial


class AddExtra : public Base {
	public:
		////////////////////////////////////////////////////
		// derived from IWebApi

		/*virtual*/ bool Process(IConnection* pIConnection)
		{
			return processAdd(pIConnection, &AddExtra::_createQueryCB);
		}

	private:
		static bool _createQueryCB(std::string& query,
				const std::string& storeId, const std::string& username,
				const Json::Value& root);
}; // class AddExtra


class DeleteExtra : public Base {
	public:
		////////////////////////////////////////////////////
		// derived from IWebApi

		/*virtual*/ bool Process(IConnection* pIConnection)
		{
			return processDelete(pIConnection, &DeleteExtra::_createQueryCB);
		}

	private:
		static bool _createQueryCB(std::string& query,
				const std::string& storeId, const std::string& username,
				const std::string& idx);
}; // class DeleteExtra

} // namespace API

