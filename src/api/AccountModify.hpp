#pragma once

#include "Common.hpp"
#include "Interface.hpp"

#include <json/json.h>


namespace Account {

class Modify : public IWebApi {
	public:
		////////////////////////////////////////////////////
		// derived from IWebApi

		virtual bool IsSupportGetMethod() const;
		virtual bool IsSupportPostMethod() const;

		virtual bool Process(IConnection* pIConnection);

	private:
		static void _makeUserQuery(std::string& query,
				const std::string& username, const Json::Value& user);
		static void _makeDeviceQuery(std::string& query,
				const std::string& username, const Json::Value& device);
}; // class Modify

} // namespace Account

