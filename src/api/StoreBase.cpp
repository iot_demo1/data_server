#include "StoreBase.hpp"
#include "DatabaseMgr.hpp"

#include <memory>
#include <cstdarg>


namespace Store {

bool
Base::processAdd(IConnection* pIConnection,
		const AddQueryCallback& callback)
{
	// parse body
	Json::Value root;

	if (!m_reader.parse(pIConnection->GetBody(), root)) {
		DSD_LOG_INFO("Failed to handle request due to malformed json syntax.");
		return pIConnection->SendResponse(400); // Bad Request
	}

	const auto bindedCallback = std::bind(callback,
			std::placeholders::_1, // for 'query'
			std::placeholders::_2, // for 'storeId'
			std::placeholders::_3, // for 'username'
			std::cref(root));

	return processImpl(pIConnection, bindedCallback);
}

bool
Base::processDelete(IConnection* pIConnection,
		const DeleteQueryCallback& callback)
{
	// parse body
	Json::Value root;

	if (!m_reader.parse(pIConnection->GetBody(), root)) {
		DSD_LOG_INFO("Failed to handle request due to malformed json syntax.");
		return pIConnection->SendResponse(400); // Bad Request
	}

	if (!root.isMember("idx"))
		return pIConnection->SendResponse(406); // Not Acceptable

	const auto bindedCallback = std::bind(callback,
			std::placeholders::_1, // for 'query'
			std::placeholders::_2, // for 'storeId'
			std::placeholders::_3, // for 'username'
			std::to_string(root["idx"].asUInt()));

	return processImpl(pIConnection, bindedCallback);
}

bool
Base::processModify(IConnection* pIConnection,
		const ModifyQueryCallback& callback)
{
	// parse body
	Json::Value root;

	if (!m_reader.parse(pIConnection->GetBody(), root)) {
		DSD_LOG_INFO("Failed to handle request due to malformed json syntax.");
		return pIConnection->SendResponse(400); // Bad Request
	}

	const auto bindedCallback = std::bind(callback,
			std::placeholders::_1, // for 'query'
			std::placeholders::_2, // for 'storeId'
			std::placeholders::_3, // for 'username'
			std::cref(root));

	return processImpl(pIConnection, bindedCallback);
}

bool
Base::processImpl(IConnection* pIConnection,
		const GenericQueryCallback& callback)
{
	const auto args = pIConnection->GetArguments();
	const auto itrStoreId = args.find("store_id");

	if (itrStoreId == args.end()) {
		DSD_LOG_INFO("PRIMARY KEY argument required: store_id");
		return pIConnection->SendResponse(406); // Not Acceptable
	}

	// create query using callback from each request handler
	std::string query;

	if (!callback(query, (*itrStoreId).second, pIConnection->GetUserName()))
		return pIConnection->SendResponse(400); // Bad Request

	// do query for DB and check result
	std::unique_ptr<Result> pRes(DatabaseMgr::_GetInstance()->Query(query));

	if (!pRes->IsSucceeded()) {
		DSD_LOG_WARN("Database error: %s", pRes->GetError());
		return pIConnection->SendResponse(500); // Internal Server Error
	}

	return pIConnection->SendResponse(200); // OK
}

} // namespace API

