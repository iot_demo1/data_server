#include "AccountCreate.hpp"
#include "DatabaseMgr.hpp"
#include "Util.hpp"

#include <memory>


namespace Account {

bool
Create::IsSupportGetMethod() const
{
	return false;
}

bool
Create::IsSupportPostMethod() const
{
	return true;
}

bool
Create::Process(IConnection* pIConnection)
{
	// validate json body
	Json::Reader reader;
	Json::Value  root;
	std::string  dbQuery;

	if (!reader.parse(pIConnection->GetBody(), root)) {
		DSD_LOG_INFO("Failed to handle request due to malformed json syntax.");
		return pIConnection->SendResponse(400); // Bad Request
	}

	if (!root.isMember("user") || !root.isMember("device"))
		return pIConnection->SendResponse(400); // Bad Request

	const Json::Value& user   = root["user"];
	const Json::Value& device = root["device"];

	if (!user.isMember("email"))
		return pIConnection->SendResponse(400); // Bad Request

	// add user information to Users table
	if (!_makeUserQuery(dbQuery, user)) {
		DSD_LOG_INFO("Failed to handle request due to invalid request.");
		return pIConnection->SendResponse(400); // Bad Request
	}

	std::unique_ptr<Result> pResUser(
			DatabaseMgr::_GetInstance()->Query(dbQuery));

	if (!pResUser->IsSucceeded()) {
		DSD_LOG_WARN("Database error: %s", pResUser->GetError());
		return pIConnection->SendResponse(500); // Internal Server Error
	}

	// add device information to Devices table
	const std::string email { user["email"].asString() };

	if (!_makeDeviceQuery(dbQuery, email, device)) {
		DSD_LOG_INFO("Failed to handle request due to invalid request.");
		return pIConnection->SendResponse(400); // Bad Request
	}

	std::unique_ptr<Result> pResDevice(
			DatabaseMgr::_GetInstance()->Query(dbQuery));

	if (!pResDevice->IsSucceeded()) {
		DSD_LOG_WARN("Database error: %s", pResDevice->GetError());
		return pIConnection->SendResponse(500); // Internal Server Error
	}

	return pIConnection->SendResponse(200); // OK
}


/*static*/ bool
Create::_makeUserQuery(std::string& query, const Json::Value& user)
{
	std::string colNames;
	std::string valExprs;

	// email CHAR(40) NOT NULL PRIMARY KEY
	if (!user.isMember("email"))
		return false;
	colNames += "email";
	valExprs += '\'' + user["email"].asString() + '\'';

	// name CHAR(40) NOT NULL
	if (!user.isMember("name"))
		return false;
	colNames += ", name";
	valExprs += ", \'" + user["name"].asString() + '\'';

	// sex ENUM('male', 'female')
	if (user.isMember("sex")) {
		colNames += ", sex";
		valExprs += ", \'" + user["sex"].asString() + '\'';
	}

	// birth_date DATE
	if (user.isMember("birth_date")) {
		colNames += ", birth_date";
		valExprs += ", \'" + Util::_ConvDateToStr(user["birth_date"]) + '\'';
	}

	// mobile CHAR(30)
	if (user.isMember("mobile")) {
		colNames += ", mobile";
		valExprs += ", \'" + user["mobile"].asString() + '\'';
	}

	query = "INSERT INTO Users (" + colNames + ") VALUE (" + valExprs + ");";

	return true;
}

/*static*/ bool
Create::_makeDeviceQuery(std::string& query,
		const std::string& email, const Json::Value& device)
{
	query = "INSERT INTO Devices VALUE (";

	// email CHAR(40) NOT NULL FOREIGN KEY REFERENCES Users(email)
	query += '\'' + email + '\'';

	// mac_addr CHAR(12) NOT NULL UNIQUE KEY
	if (!device.isMember("mac_addr"))
		return false;
	query += ", \'" + device["mac_addr"].asString() + '\'';

	// registration_id VARCHAR(4096) NOT NULL
	if (!device.isMember("registration_id"))
		return false;
	query += ", \'" + device["registration_id"].asString() + "\');";

	return true;
}

} // namespace Account

