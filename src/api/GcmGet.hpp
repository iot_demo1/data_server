#pragma once

#include "Common.hpp"
#include "Interface.hpp"

#include <set>


namespace GCM {

class Get : public IWebApi {
	public:
		////////////////////////////////////////////////////
		// derived from IWebApi

		/*virtual*/ bool IsSupportGetMethod() const;
		/*virtual*/ bool IsSupportPostMethod() const;

		/*virtual*/ bool Process(IConnection* pIConnection);

	private:
		static bool _getRegistraionInfos(KeyValueList& regInfos,
				const MultiKeyValueList& args);
		static bool _makeResponseBody(std::string& body,
				const std::string& storeId, const KeyValueList& regInfos);

		static bool _getStoreInfo(std::string& storeName, std::string& category,
				const std::string& storeId);
		static bool _getFavorites(std::set<std::string>& favorites,
				const std::string& email);
		static bool _getMsg(std::string& msg, const std::string& storeId);
}; // class Get

} // namespace GCM

