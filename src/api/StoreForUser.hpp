#pragma once

#include "StoreBase.hpp"


namespace Store {

class AddPictures : public Base {
	public:
		////////////////////////////////////////////////////
		// derived from IWebApi

		/*virtual*/ bool Process(IConnection* pIConnection)
		{
			return processAdd(pIConnection, &AddPictures::_createQueryCB);
		}

	private:
		static bool _createQueryCB(std::string& query,
				const std::string& storeId, const std::string& username,
				const Json::Value& root);
}; // class AddPictures


class DeletePicture : public Base {
	public:
		////////////////////////////////////////////////////
		// derived from IWebApi

		/*virtual*/ bool Process(IConnection* pIConnection)
		{
			return processDelete(pIConnection, &DeletePicture::_createQueryCB);
		}

	private:
		static bool _createQueryCB(std::string& query,
				const std::string& storeId, const std::string& username,
				const std::string& idx);
}; // class DeletePicture


class AddReview : public Base {
	public:
		////////////////////////////////////////////////////
		// derived from IWebApi

		/*virtual*/ bool Process(IConnection* pIConnection)
		{
			return processAdd(pIConnection, &AddReview::_createQueryCB);
		}

	private:
		static bool _createQueryCB(std::string& query,
				const std::string& storeId, const std::string& username,
				const Json::Value& root);
}; // class AddReview


class DeleteReview : public Base {
	public:
		////////////////////////////////////////////////////
		// derived from IWebApi

		/*virtual*/ bool Process(IConnection* pIConnection)
		{
			return processDelete(pIConnection, &DeleteReview::_createQueryCB);
		}

	private:
		static bool _createQueryCB(std::string& query,
				const std::string& storeId, const std::string& username,
				const std::string& idx);
}; // class DeleteReview


class AddTag : public Base {
	public:
		////////////////////////////////////////////////////
		// derived from IWebApi

		/*virtual*/ bool Process(IConnection* pIConnection)
		{
			return processAdd(pIConnection, &AddTag::_createQueryCB);
		}

	private:
		static bool _createQueryCB(std::string& query,
				const std::string& storeId, const std::string& username,
				const Json::Value& root);
}; // class AddTag


class DeleteTag : public Base {
	public:
		////////////////////////////////////////////////////
		// derived from IWebApi

		/*virtual*/ bool Process(IConnection* pIConnection)
		{
			return processDelete(pIConnection, &DeleteTag::_createQueryCB);
		}

	private:
		static bool _createQueryCB(std::string& query,
				const std::string& storeId, const std::string& username,
				const std::string& idx);
}; // class DeleteTag

} // namespace Store

