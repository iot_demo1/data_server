#pragma once

#include "Common.hpp"
#include "Interface.hpp"

#include <microhttpd.h>


class ConnectionMgr {
	public:
		static ConnectionMgr* _GetInstance();
		static void           _DeleteInstance();

		void SetListener(IConnectionListener* pIListener);

		bool Start(uint16_t port);
		void Stop();

	private:
		static ConnectionMgr* s_pInst;

		// callback functions
		static void _panicHandler(void* cls,
				const char* file, unsigned int line, const char* reason)
		{
			reinterpret_cast<ConnectionMgr*>(cls)->panicHandler(
					file, line, reason);
		}
		static int _accessHandler(void* cls,
				MHD_Connection* connection,
				const char* url, const char* method, const char* version,
				const char* upload_data, size_t* upload_data_size,
				void** con_cls)
		{
			return reinterpret_cast<ConnectionMgr*>(cls)->accessHandler(
					connection, url, method,
					upload_data, upload_data_size, con_cls);
		}
		static void _requestCompleted(void* cls,
				MHD_Connection* connection, void** con_cls,
				MHD_RequestTerminationCode toe)
		{
			return reinterpret_cast<ConnectionMgr*>(cls)->requestCompleted(
					connection, con_cls, toe);
		}

		ConnectionMgr();

		void panicHandler(const std::string& file, uint32_t line,
				const std::string& reason);
		int  accessHandler(MHD_Connection* pConn,
				const std::string& url, const std::string& method,
				const char* pUploadData, size_t* pcbUploadData,
				void** ppConCls);
		void requestCompleted(MHD_Connection* pConn, void** ppConCls,
				MHD_RequestTerminationCode toe);

	private:
		MHD_Daemon* m_pDaemon;
		IConnectionListener* m_pIListener;
}; // class ConnectionMgr

