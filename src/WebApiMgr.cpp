#include "WebApiMgr.hpp"
#include "api/AccountCreate.hpp"
#include "api/AccountGet.hpp"
#include "api/AccountModify.hpp"
#include "api/GcmGet.hpp"
#include "api/StoreGet.hpp"
#include "api/StoreForUser.hpp"
#include "api/StoreForOwner.hpp"


// static class member variable declaration
/*static*/ WebApiMgr* WebApiMgr::s_pInst = nullptr;


/*static*/ WebApiMgr*
WebApiMgr::_GetInstance()
{
	if (!s_pInst)
		s_pInst = new WebApiMgr;

	return s_pInst;
}

/*static*/ void
WebApiMgr::_DeleteInstance()
{
	if (!s_pInst)
		return;

	delete s_pInst;
	s_pInst = nullptr;
}

IWebApi*
WebApiMgr::GetApiInterface(
		const std::string& url, const std::string& method) const
{
	auto apiItem = m_apiList.find(url);

	if (apiItem == m_apiList.end())
		return nullptr;

	return apiItem->second;
}


WebApiMgr::WebApiMgr()
{
	// account APIs
	m_apiList["/account/create"]            = new Account::Create;
	m_apiList["/account/get"]               = new Account::Get;
	m_apiList["/account/modify"]            = new Account::Modify;

	// GCM API
	m_apiList["/gcm/get"]                   = new GCM::Get;

	// store API (1) - get
	m_apiList["/store/get"]                 = new Store::Get;
	// store API (2) - for normal users
	m_apiList["/store/picture/add"]         = new Store::AddPictures;
	m_apiList["/store/picture/delete"]      = new Store::DeletePicture;
	m_apiList["/store/review/add"]          = new Store::AddReview;
	m_apiList["/store/review/delete"]       = new Store::DeleteReview;
	m_apiList["/store/tag/add"]             = new Store::AddTag;
	m_apiList["/store/tag/delete"]          = new Store::DeleteTag;
	// store API (3) - for store admin
	m_apiList["/store/basic/modify"]        = new Store::ModifyBasic;
	m_apiList["/store/businesshour/modify"] = new Store::ModifyBusinessHour;
	m_apiList["/store/feature/modify"]      = new Store::ModifyFeature;
	m_apiList["/store/promotion/add"]       = new Store::AddPromotion;
	m_apiList["/store/promotion/delete"]    = new Store::DeletePromotion;
	m_apiList["/store/social/modify"]       = new Store::ModifySocial;
	m_apiList["/store/extra/add"]           = new Store::AddExtra;
	m_apiList["/store/extra/delete"]        = new Store::DeleteExtra;

	DSD_LOG_INFO("WebApiMgr instance created.");
}

WebApiMgr::~WebApiMgr()
{
	// delete API instances
	for (auto e: m_apiList)
		delete e.second;

	m_apiList.clear();
}

