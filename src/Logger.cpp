#include "Logger.hpp"


/*static*/ void
Logger::_Init(bool bVerbose, bool bDaemon/*=false*/)
{
	int option = LOG_PID | LOG_NDELAY;

	if (!bVerbose)
		setlogmask(LOG_UPTO(LOG_WARNING));
	if (!bDaemon)
		option |= LOG_PERROR;

	openlog(nullptr, option, LOG_USER);
}

/*static*/ void
Logger::_Fini()
{
	closelog();
}

