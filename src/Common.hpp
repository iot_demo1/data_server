#pragma once

#include <system_error>

#include "Logger.hpp"

#define THROW_SYSTEM_ERROR(desc) \
{ \
	DSD_LOG_ERROR("Exception occurred! (System error)") \
	throw std::system_error(errno, std::system_category(), desc); \
}

#define THROW_RUNTIME_ERROR(desc) \
{ \
	DSD_LOG_ERROR("Exception occurred! (Runtime error)") \
	throw std::runtime_error(desc); \
}


typedef std::map<std::string, std::string>      KeyValueList;
typedef std::multimap<std::string, std::string> MultiKeyValueList;

struct Date {
	uint32_t year;
	uint32_t month;
	uint32_t day;
};

struct Time {
	uint32_t hour;
	uint32_t minute;
};

