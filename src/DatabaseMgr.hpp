#pragma once

#include "Common.hpp"
#include "Interface.hpp"
#include "DatabaseUtil.hpp"

#include <mysql.h>


class DatabaseMgr {
	public:
		static DatabaseMgr* _GetInstance();
		static void         _DeleteInstance();

		bool Connect(const std::string& database);

		Result* Query(const std::string& query);

	private:
		static DatabaseMgr* s_pInst;

		DatabaseMgr();
		~DatabaseMgr();

	private:
		MYSQL* m_pConn;
}; // class DatabaseMgr

