#pragma once

#include "Common.hpp"
#include "Response.hpp"


class IConnection {
	public:
		virtual ~IConnection() {}	// virtual dtor for derived class

		virtual const std::string&  GetUrl() const = 0;
		virtual const std::string&  GetMethod() const = 0;
        virtual const std::string&  GetUserName() const = 0;
        virtual const std::string&  GetPassword() const = 0;
		virtual const KeyValueList& GetHeader() const = 0;
		virtual const MultiKeyValueList& GetArguments() const = 0;
		virtual const std::string&  GetBody() const = 0;

		virtual bool SendResponse(uint32_t statusCode) = 0;
		virtual bool SendResponse(uint32_t statusCode, const Response& response) = 0;
}; // interface IConnection


class IConnectionListener {
	public:
		// invoked when HTTP daemon successfully started
		virtual void OnStarted() = 0;
		// invoked when HTTP daemon stopped completely
		virtual void OnStopped() = 0;

		// invoked when the fatal error occurred
		virtual void OnPanic(
				const std::string& file, uint32_t line,
				const std::string& reason) = 0;

		// invoked when an new request received from the client
		virtual bool OnConnected(IConnection* pIConnection) = 0;
		// invoked when received HTTP body completely
		virtual bool OnReceiveDone(IConnection* pIConnection) = 0;
		// invoked when finally received result from client
		virtual void OnCompleted(IConnection* pIConnection, int termCode) = 0;
}; // interface IConnectionListener


class IWebApi {
	public:
		virtual ~IWebApi() {}	// virtual dtor for derived class

		// support HTTP GET?
		virtual bool IsSupportGetMethod() const = 0;
		// support HTTP POST?
		virtual bool IsSupportPostMethod() const = 0;

		// actual processing of request and return response 
		virtual bool Process(IConnection* pIConnection) = 0;
}; // interface IWebApi

