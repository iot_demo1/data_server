#include "ServerMain.hpp"
#include "ConnectionMgr.hpp"
#include "DatabaseMgr.hpp"
#include "WebApiMgr.hpp"

#include <thread>


////////////////////////////////////////////////////////////////////////////////
// ServerMain::ConnectionListener class

void
ServerMain::ConnectionListener::OnStarted()
{
	DSD_LOG_NOTICE("ConnectionMgr started.");
}

void
ServerMain::ConnectionListener::OnStopped()
{
	DSD_LOG_NOTICE("ConnectionMgr stopped.");
}

void
ServerMain::ConnectionListener::OnPanic(const std::string& file, uint32_t line,
		const std::string& reason)
{
	Logger::_Log(LOG_ERR, file.c_str(), line, "Fatal error occurred in MHD!");
	throw std::runtime_error(reason);
}

bool
ServerMain::ConnectionListener::OnConnected(IConnection* pIConnection)
{
	DSD_LOG_INFO("New connection established.");
	DSD_LOG_INFO("URL=(%s), method=(%s)",
			pIConnection->GetUrl().c_str(), pIConnection->GetMethod().c_str());

	// POST request validation
	if (pIConnection->GetMethod() == "POST") {
		const KeyValueList& header = pIConnection->GetHeader();
		const auto contentType = header.find(MHD_HTTP_HEADER_CONTENT_TYPE);

		// all POST request must have Content-Type field
		if (contentType == header.end()) {
			DSD_LOG_WARN("No Content-Type field in the request header.");
			return pIConnection->SendResponse(406); // Not Acceptable
		}

		// all POST request must have a body of JSON type
		if (contentType->second != "application/json") {
			DSD_LOG_WARN("Content-Type is not 'application/json'.");
			return pIConnection->SendResponse(406); // Not Acceptable
		}
	}

	return true;
}

bool
ServerMain::ConnectionListener::OnReceiveDone(IConnection* pIConnection)
{
	DSD_LOG_INFO("Receiving HTTP body complete.");

	const std::string& url    = pIConnection->GetUrl();
	const std::string& method = pIConnection->GetMethod();

	IWebApi* pIWebApi = WebApiMgr::_GetInstance()->GetApiInterface(url, method);

	if (!pIWebApi) {
		DSD_LOG_WARN("URL not found (%s).", url.c_str());
		return pIConnection->SendResponse(404); // Not Found
	}
	if (((method == "GET") && !pIWebApi->IsSupportGetMethod()) ||
			((method == "POST") && !pIWebApi->IsSupportPostMethod())) {
		DSD_LOG_WARN("Not supported method for URL (%s message for %s).",
				method.c_str(), url.c_str());
		return pIConnection->SendResponse(405); // Method Not Allowed
	}

	DSD_LOG_DEBUG("Start request processing ...");

	if (!pIWebApi)
		THROW_RUNTIME_ERROR("No API interface founded.");

	if (!pIWebApi->Process(pIConnection)) {
		DSD_LOG_WARN("Failed to handle request: HTTP %s message for %s",
				method.c_str(), url.c_str());
		return false;
	}

	return true;
}

void
ServerMain::ConnectionListener::OnCompleted(IConnection* pIConnection,
		int termCode)
{
	DSD_LOG_INFO("Request has been completed.");

	switch (termCode) {
		case 1: // Error
			DSD_LOG_WARN("Generic error occurred.");
			break;
		case 2: // Timeout
			DSD_LOG_WARN("Timeout occurred.");
			break;
		case 3: // Daemon shutdown
			DSD_LOG_WARN("MHD was being shut down.");
			break;
		case 4: // Connection closed by peer
			DSD_LOG_WARN("Connection closed by peer.");
			break;
		case 5: // Aborted by client
			DSD_LOG_WARN("Client terminated the connection.");
			break;
		default:
			break;
	}
}


////////////////////////////////////////////////////////////////////////////////
// ServerMain::ConnectionListener class

// static class member variable declaration
/*static*/ ServerMain* ServerMain::s_pInst = nullptr;


/*static*/ void
ServerMain::_Run(uint16_t port)
{
	if (!s_pInst)
		s_pInst = new ServerMain;

	// run main loop - this will block the main thread
	s_pInst->run(port);

	delete s_pInst;
	s_pInst = nullptr;

	DSD_LOG_INFO("ServerMain stopped.");
}

/*static*/ void
ServerMain::_Quit()
{
	if (!s_pInst)
		return;

	// MainTask::quitLoop() should be invoked by other thread
	std::thread t([]() { s_pInst->quit(); });

	t.detach();
}


ServerMain::ServerMain()
{
	DSD_LOG_DEBUG(__PRETTY_FUNCTION__);

	// create global singleton objects
	ConnectionMgr::_GetInstance()->SetListener(&m_connListener);
	DatabaseMgr::_GetInstance();
	WebApiMgr::_GetInstance();
}

ServerMain::~ServerMain()
{
	// delete global singleton objects
	WebApiMgr::_DeleteInstance();
	DatabaseMgr::_DeleteInstance();
	ConnectionMgr::_DeleteInstance();

	DSD_LOG_DEBUG(__PRETTY_FUNCTION__);
}

void
ServerMain::run(uint16_t port)
{
	const std::chrono::seconds intervalSec(1);

	// connect database
	if (!DatabaseMgr::_GetInstance()->Connect("iot_demo1"))
		THROW_RUNTIME_ERROR("Failed to start DatabaseMgr.");

	// start HTTP server for accepting connections from client
	if (!ConnectionMgr::_GetInstance()->Start(port))
		THROW_RUNTIME_ERROR("Failed to start ConnectionMgr.");

	// dummy loop
	m_bLoop = true;

	while (m_bLoop) {
		std::this_thread::sleep_for(intervalSec);
	}

	// stop HTTP server
	ConnectionMgr::_GetInstance()->Stop();
}

void
ServerMain::quit()
{
	m_bLoop = false;
}

