#include "Connection.hpp"


Connection::Connection(MHD_Connection* pConn,
		const std::string& url, const std::string& method)
	: m_pConn  { pConn }
	, m_url    { url }
	, m_method { method }
{
	DSD_LOG_DEBUG(__PRETTY_FUNCTION__);

	// get basic authentication information
	char* pszUser;
	char* pszPass = nullptr;

	pszUser = MHD_basic_auth_get_username_password(m_pConn, &pszPass);
	if (pszUser) {
		m_username = pszUser;
		free(pszUser);
	}
	if (pszPass) {
		m_password = pszPass;
		free(pszPass);
	}

	// iterating HTTP header and arguments
	MHD_get_connection_values(m_pConn, MHD_HEADER_KIND,
			&Connection::_iterateValue, &m_header);
	MHD_get_connection_values(m_pConn, MHD_GET_ARGUMENT_KIND,
			&Connection::_iterateValue, &m_arguments);
}

void
Connection::PushBodyContent(const std::string& content)
{
	DSD_LOG_DEBUG("Pushed body content=\n%s", content.c_str());
	m_body += content;
}

bool
Connection::SendResponse(uint32_t statusCode)
{
	return SendResponse(statusCode, Response());
}

bool
Connection::SendResponse(uint32_t statusCode, const Response& response)
{
	DSD_LOG_INFO("Sending response ...");

	return (MHD_queue_response(m_pConn, statusCode,
			static_cast<MHD_Response*>(response)) == MHD_YES);
}


/*static*/ int
Connection::_iterateValue(void* cls, MHD_ValueKind kind,
		const char* key, const char* value)
{
	DSD_LOG_DEBUG("iterating connection values ... "
			"kind=(%d) / key=(%s) / value=(%s)", kind, key, value);

	if (kind == MHD_HEADER_KIND) {
		KeyValueList* pList = reinterpret_cast<KeyValueList*>(cls);

		(*pList)[key] = value;
	}
	else if (kind == MHD_GET_ARGUMENT_KIND) {
		MultiKeyValueList* pList = reinterpret_cast<MultiKeyValueList*>(cls);

		pList->insert(std::make_pair(key, value));
	}
	else {
		DSD_LOG_INFO("Unknown value kind (%d).", kind);
		return MHD_NO;
	}

	return MHD_YES;
}

