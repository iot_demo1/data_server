#pragma once

#include "Common.hpp"


// forward declaration
struct MHD_Response;


class Response {
	public:
		Response();
		Response(const std::string& body);
		~Response();

		bool AddHeader(const std::string& key, const std::string& value);

		explicit operator MHD_Response*() const
		{
			return m_pResponse;
		}

	private:
		MHD_Response* m_pResponse;
}; // class Response

