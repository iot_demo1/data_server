#include "DatabaseMgr.hpp"


// static class member variable declaration
/*static*/ DatabaseMgr* DatabaseMgr::s_pInst = nullptr;


/*static*/ DatabaseMgr*
DatabaseMgr::_GetInstance()
{
	if (!s_pInst)
		s_pInst = new DatabaseMgr;

	return s_pInst;
}

/*static*/ void
DatabaseMgr::_DeleteInstance()
{
	if (!s_pInst)
		return;

	delete s_pInst;
	s_pInst = nullptr;
}

bool
DatabaseMgr::Connect(const std::string& database)
{
	DSD_LOG_DEBUG("Connecting Database (name: %s) ...", database.c_str());

	if (mysql_real_connect(m_pConn, "localhost",
				"root", nullptr, database.c_str(),
				0, nullptr, 0) == nullptr) {
		DSD_LOG_ERROR("mysql_real_connect() failed: %s", mysql_error(m_pConn));
		return false;
	}

	return true;
}

Result*
DatabaseMgr::Query(const std::string& query)
{
	DSD_LOG_DEBUG("[SQL Query] %s", query.c_str());

	return new Result(m_pConn, query);
}


DatabaseMgr::DatabaseMgr()
{
	m_pConn = mysql_init(nullptr);
	if (!m_pConn)
		THROW_RUNTIME_ERROR("mysql_init() failed: Insufficient memory");

	// set charset autodetected based on the OS setting
	mysql_options(m_pConn,
			MYSQL_SET_CHARSET_NAME, MYSQL_AUTODETECT_CHARSET_NAME);
	// automatic reconnect
	my_bool bReconnect = true;

	mysql_options(m_pConn, MYSQL_OPT_RECONNECT, &bReconnect);

	DSD_LOG_INFO("DatabaseMgr instance created.");
}

DatabaseMgr::~DatabaseMgr()
{
	mysql_close(m_pConn);
	m_pConn = nullptr;
}

