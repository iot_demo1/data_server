#include "DatabaseMgr.hpp"

#include <memory>

#include <json/json.h>
#include <mysql/mysqld_error.h>


namespace API {

static int
_InsertBasicInfo(int& storeId, const Json::Value& root)
{
	if (!root.isMember("basic")) {
		DSD_LOG_WARN("No basic info in the request (Required).");
		return 400; // Bad Request
	}

	Json::Value basic = root["basic"];
	std::string query;

	// all fields are mandatory
	if (!basic.isMember("category") || !basic.isMember("name") ||
		!basic.isMember("address") || !basic.isMember("contact"))
		return 400; // Bad Request

	query = "INSERT INTO StoreBasic(category, name, address, contact) VALUE(";
	query += '\'' + basic["category"].asString() + "\', ";
	query += '\'' + basic["name"].asString() + "\', ";
	query += '\'' + basic["address"].asString() + "\', ";
	query += '\'' + basic["contact"].asString() + "\');";

	std::unique_ptr<Result> pRes1(DatabaseMgr::_GetInstance()->Query(query));

	if (!pRes1->IsSucceeded()) {
		DSD_LOG_WARN("Database error: %s", pRes1->GetError());
		return 500; // Internal Server Error
	}

	// query again store ID which is auto-generated
	query = "SELECT MAX(store_id) FROM StoreBasic;";

	std::unique_ptr<Result> pRes2(DatabaseMgr::_GetInstance()->Query(query));

	if (!pRes2->IsSucceeded()) {
		DSD_LOG_WARN("Database error: %s", pRes2->GetError());
		return 500; // Internal Server Error
	}

	// basic info should be unique by store
	if (pRes2->GetNumOfRows() != 1) {
		DSD_LOG_ERROR("Duplicated ID detected. DB integrity is corrupted.");
		return 500; // Internal Server Error
	}

	// return store ID
	storeId = std::atoi((*pRes2)[0]["store_id"].c_str());

	return 200; // OK
}


bool
AddNewStore::IsSupportGetMethod() const
{
	return false;
}

bool
AddNewStore::IsSupportPostMethod() const
{
	return true;
}

bool
AddNewStore::Process(IConnection* pIConnection)
{
	Json::Reader reader;
	Json::Value  root;

	if (!reader.parse(pIConnection->GetBody(), root)) {
		DSD_LOG_INFO("Failed to handle request due to malformed json syntax.");
		return pIConnection->SendResponse(400); // Bad Request
	}

	int storeId = std::atoi(itrStoreId->second.c_str());
	std::unique_ptr<Result> pRes(_QueryStoreId(storeId));

	if (!pRes->IsSucceeded()) {
		DSD_LOG_WARN("Database error: %s", pRes->GetError());
		return pIConnection->SendResponse(500); // Internal Server Error
	}

	switch (pRes->GetNumOfRows()) {
		case 0:
			DSD_LOG_NOTICE("Query with the store ID not exist.");
			status = 406; // Not Acceptable
			break;
		case 1:
			status = 200; // OK
			break;
		default:
			DSD_LOG_ERROR("Duplicated ID detected. DB integrity is corrupted.");
			status = 500; // Internal Server Error
			break;
	}

	if (status == 200) {
		if (200 != (status = _InsertPictureData(storeId, root)))
			goto done;
		if (200 != (status = _InsertBusinessHour(storeId, root)))
			goto done;
		if (200 != (status = _InsertFeature(storeId, root)))
			goto done;
		if (200 != (status = _InsertPromotion(storeId, root)))
			goto done;
		if (200 != (status = _InsertSocial(storeId, root)))
			goto done;
		if (200 != (status = _InsertReview(storeId, root)))
			goto done;
		if (200 != (status = _InsertTag(storeId, root)))
			goto done;
		if (200 != (status = _InsertExtra(storeId, root)))
			goto done;
	}

done:
	return pIConnection->SendResponse(status);
}

} // namespace API

