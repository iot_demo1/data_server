# README #

## 1. Dependencies (Ubuntu의 package명 기준) ##

* **g++** : GNU C++ 컴파일러
* **libjsoncpp-dev** : C++용 JSON 읽기/쓰기 라이브러리
* **libmysqlclient-dev** : MySQL 데이터베이스 개발 라이브러리
* **libmicrohttpd-dev** : micro http daemon

## 2. How to build ##

단지 make를 실행하면 된다. 성공하면 top 디렉토리에 **dataserverd** 실행파일이 생성된다.


## 3. 실행 옵션 ##

*     --no-daemon : 강제로 일반 모드로 실행한다.
* -v, --verbose   : 디버깅 로그를 출력한다.
* -h, --help      : 실행 도움말을 보여준다.